package de.laulajatar.model.room;

import de.laulajatar.model.user.BingoUser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.*;
import java.util.stream.Collectors;


import static org.assertj.core.api.Assertions.*;

@RunWith(Parameterized.class)
public class BingoBoardParameterizedTest {
    private final String[] fields = {"0/0", "0/1", "0/2", "0/3", "0/4",
            "1/0", "1/1", "1/2", "1/3", "1/4",
            "2/0", "2/1", "2/2", "2/3", "2/4",
            "3/0", "3/1", "3/2", "3/3", "3/4",
            "4/0", "4/1", "4/2", "4/3", "4/4"};

    private List<String> termsToCall;

    @Parameterized.Parameters
    public static Collection data() {
        String[] v1 = {"0/0", "0/1", "0/2", "0/3"};
        String[] v2 = { "0/0", "0/1", "0/2", "0/3", "1/0", "1/1", "1/2", "1/3"};
        String[] v3 = { "0/0", "1/1", "2/2", "3/3"};
        String[] v4 = { "0/0", "1/1", "2/2", "3/3", "0/1", "0/2", "0/3"};
        return Arrays.asList(new Object[][] {
                { v1},
                { v2 },
                { v3 },
                { v4 }
        });
    }



    public BingoBoardParameterizedTest(String[] termsToCall) {
        this.termsToCall = Arrays.asList(termsToCall);
    }

    @Test
    public void executeTest() {
        List<BingoField> bingoFields = Arrays.stream(fields).map(BingoField::new).collect(Collectors.toList());
        // Do not set the list here, will be shuffled.
        BingoBoard board = new BingoBoard();
        board.setFieldList(bingoFields);
        board.setBingo(false);
        this.termsToCall.forEach(board::callTerm);
        assertThat(board.getBingo()).isFalse();

    }

}
