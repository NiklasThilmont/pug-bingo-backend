package de.laulajatar.model.room;

import de.laulajatar.model.user.BingoUser;
import org.junit.Test;

import java.util.Arrays;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

public class BingoRoomTest {

    private final String[] fields = {"0/0", "0/1", "0/2", "0/3", "0/4",
            "1/0", "1/1", "1/2", "1/3", "1/4",
            "2/0", "2/1", "2/2", "2/3", "2/4",
            "3/0", "3/1", "3/2", "3/3", "3/4",
            "4/0", "4/1", "4/2", "4/3", "4/4"};

    /**
     * Makes sure that a user can join a room
     */
    @Test
    public void testJoinAndLeaveRoom() {
        BingoRoom bingoRoom = new BingoRoom("test", null, Arrays.asList(fields));
        BingoUser user = new BingoUser("id", "test", null);
        bingoRoom.addUserToRoom(user);
        //
        assertThat(bingoRoom.getParticipantList()).isNotEmpty();
        Optional<BingoParticipant> p = bingoRoom.getParticipantList().stream().findFirst();
        assertThat(p).isPresent();
        assertThat(p.get().getBingoBoard().getFieldList().size()).isEqualTo(fields.length);

        bingoRoom.removeUserFromRoom("id");
        assertThat(bingoRoom.getParticipantList()).isEmpty();
    }
}