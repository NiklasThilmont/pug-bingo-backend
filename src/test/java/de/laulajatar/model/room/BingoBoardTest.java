package de.laulajatar.model.room;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Niklas on 03.06.2017.
 */
public class BingoBoardTest {
    private final String[] fields = {"0/0", "0/1", "0/2", "0/3", "0/4",
                        "1/0", "1/1", "1/2", "1/3", "1/4",
                        "2/0", "2/1", "2/2", "2/3", "2/4",
                        "3/0", "3/1", "3/2", "3/3", "3/4",
                        "4/0", "4/1", "4/2", "4/3", "4/4"};

    private BingoBoard createPredicableBingoBoard(String[] fields) {
        List<BingoField> res = Arrays.stream(fields).map(BingoField::new).collect(Collectors.toList());
        // Use the default to avoid shuffeling
        BingoBoard bingoBoard = new BingoBoard();
        bingoBoard.setFieldList(res);
        bingoBoard.setBingo(false);
        return bingoBoard;
    }

    @Test
    public void testCallTermExpectedBingoRow() throws Exception {
        BingoBoard bingoBoard = createPredicableBingoBoard(fields);
        bingoBoard.callTerm("0/0");
        bingoBoard.callTerm("0/1");
        bingoBoard.callTerm("0/2");
        bingoBoard.callTerm("0/3");
        bingoBoard.callTerm("0/4");
        assertThat(bingoBoard.getBingo()).isTrue();
    }
    @Test
    public void testCallTermExpectedBingoCol() {
        BingoBoard bingoBoard = createPredicableBingoBoard(fields);
        bingoBoard.callTerm("0/0");
        bingoBoard.callTerm("1/0");
        bingoBoard.callTerm("2/0");
        bingoBoard.callTerm("3/0");
        bingoBoard.callTerm("4/0");
        assertThat(bingoBoard.getBingo()).isTrue();
    }
    @Test
    public void testCallTermExpectDiagonalBingo1() {
        BingoBoard bingoBoard =createPredicableBingoBoard(fields);
        bingoBoard.callTerm("4/0");
        bingoBoard.callTerm("3/1");
        bingoBoard.callTerm("2/2");
        bingoBoard.callTerm("1/3");
        bingoBoard.callTerm("0/4");
        assertThat(bingoBoard.getBingo()).isTrue();
    }
    @Test
    public void testCallTermExpectDiagonalBingo2() {
        BingoBoard bingoBoard = createPredicableBingoBoard(fields);
        bingoBoard.callTerm("0/0");
        bingoBoard.callTerm("1/1");
        bingoBoard.callTerm("2/2");
        bingoBoard.callTerm("3/3");
        bingoBoard.callTerm("4/4");
        assertThat(bingoBoard.getBingo()).isTrue();
    }

}