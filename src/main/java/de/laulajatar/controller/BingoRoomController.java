package de.laulajatar.controller;

import de.laulajatar.exception.AccessDeniedException;
import de.laulajatar.exception.BadRequestException;
import de.laulajatar.exception.NotFoundException;
import de.laulajatar.model.room.BingoRoom;
import de.laulajatar.model.room.BingoRoomInfo;
import de.laulajatar.model.user.BingoUser;
import de.laulajatar.persistence.BingoRoomRepository;
import de.laulajatar.persistence.BingoUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/rooms")
public class BingoRoomController {
    private final BingoRoomRepository bingoRoomRepository;
    private final BingoUserRepository bingoUserRepository;

    @Autowired
    public BingoRoomController(BingoRoomRepository bingoRoomRepository, BingoUserRepository bingoUserRepository) {
        this.bingoRoomRepository = bingoRoomRepository;
        this.bingoUserRepository = bingoUserRepository;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/{id}")
    public ResponseEntity<BingoRoom> getRoom(@PathVariable(value = "id") String id,
                                             @RequestHeader(required = false, value = "password") String roomPass) {
        BingoRoom room = bingoRoomRepository.findOne(id);
        if(room == null) throw new NotFoundException();
        if(!room.mayAccess(roomPass)) throw new AccessDeniedException();
        return ResponseEntity.ok(room);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/{id}/user/{userid}")
    public ResponseEntity<BingoRoom> joinRoom(@PathVariable(value = "id") String id,
                                              @PathVariable(value = "userid") String userId,
                                              @RequestHeader(required = false, value = "password") String roomPass) {
        BingoRoom room = bingoRoomRepository.findOne(id);
        if(room == null) throw new NotFoundException();
        if(!room.mayAccess(roomPass)) throw new AccessDeniedException();

        BingoUser user = bingoUserRepository.findOne(userId);
        if(user == null) throw new BadRequestException();
        if(!room.userParticipates(user)) {
            room.addUserToRoom(user);
            bingoRoomRepository.save(room);
        }
        return ResponseEntity.ok(room);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/{id}/user/{userid}")
    @Transactional
    public ResponseEntity<BingoRoom> leaveRoom(@PathVariable(value = "id") String id,
                                              @PathVariable(value = "userid") String userId,
                                              @RequestHeader(required = false, value = "password") String roomPass) {
        BingoRoom room = bingoRoomRepository.findOne(id);
        if(room == null) throw new NotFoundException();
        if(!room.mayAccess(roomPass)) throw new AccessDeniedException();
        room.removeUserFromRoom(userId);
        bingoRoomRepository.save(room);
        return ResponseEntity.ok(room);
    }


    @RequestMapping(method = RequestMethod.GET, path = "/{id}/info")
    public ResponseEntity<BingoRoomInfo> getInfo(@PathVariable(value= "id") String id) {
        BingoRoom room = bingoRoomRepository.findOne(id);
        if(room == null) throw new NotFoundException();
        return ResponseEntity.ok(new BingoRoomInfo(room));
    }

    @RequestMapping(method = RequestMethod.GET, path = "")
    public ResponseEntity<List<String> > getAllRoomIds() {
        Iterable<BingoRoom> rooms = bingoRoomRepository.findAll();
        List<String> roomIds = new ArrayList<String>();
        rooms.forEach(bingoRoom -> roomIds.add(bingoRoom.getId()));
        return ResponseEntity.ok(roomIds);
    }


    @RequestMapping(method = RequestMethod.POST, path = "/", consumes = "application/json", produces = "application/json")
    public ResponseEntity<BingoRoom> addRoom(@RequestBody BingoRoom room) {
        // Create a new room from it.
        if(room.getTerms() == null) throw new BadRequestException("Terms must not be null.");
        if(room.getTerms().size() != 25) throw new BadRequestException("There must be exactly 25 terms.");
        BingoRoom newRoom = new BingoRoom(room.getRoomName(), room.getPassword(), room.getTerms());
        newRoom = bingoRoomRepository.save(newRoom);
        return ResponseEntity.ok(newRoom);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/{id}")
    public ResponseEntity deleteRoom(@PathVariable(value = "id") String id,
                                     @RequestHeader(required = false, value = "password") String roomPass) {
        BingoRoom room = bingoRoomRepository.findOne(id);
        if(room == null) throw new BadRequestException("Room with id=" + id + " does not exist");
        if(!room.mayAccess(roomPass)) throw new AccessDeniedException();
        bingoRoomRepository.delete(room);
        return ResponseEntity.noContent().build();
    }

    @RequestMapping(method = RequestMethod.POST, path = "/{id}/terms")
    public ResponseEntity<BingoRoom> callTerm(@RequestBody Map<String, String> body,
                                              @PathVariable String id,
                                              @RequestHeader(required = false, value = "password") String roomPass) {
        BingoRoom room = bingoRoomRepository.findOne(id);
        if(room == null) throw new NotFoundException();
        if(!room.mayAccess(roomPass)) throw new AccessDeniedException();
        String term = body.get("term");
        if(term == null) throw new BadRequestException("Invalid message body.");
        room.callTermToAllParticipants(term);
        bingoRoomRepository.save(room);
        return ResponseEntity.ok(room);
    }
}
