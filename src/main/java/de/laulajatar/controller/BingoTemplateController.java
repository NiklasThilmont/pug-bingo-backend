package de.laulajatar.controller;

import de.laulajatar.model.template.BingoTemplate;
import de.laulajatar.persistence.BingoTemplateRepository;
import org.apache.tomcat.util.http.fileupload.util.Streams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping("/templates")
public class BingoTemplateController {
    private final BingoTemplateRepository bingoTemplateRepository;

    @Autowired
    public BingoTemplateController(BingoTemplateRepository bingoTemplateRepository) {
        this.bingoTemplateRepository = bingoTemplateRepository;
    }

    @RequestMapping("")
    public List<String> getAllTemplateIds() {
        return StreamSupport.stream(bingoTemplateRepository.findAll().spliterator(), false)
                .map(BingoTemplate::getId)
                .collect(Collectors.toList());

    }

    @RequestMapping("/{id}")
    public BingoTemplate getById(@PathVariable ("id") String id) {
        return bingoTemplateRepository.findOne(id);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/")
    public BingoTemplate createNewTemplate(@RequestBody BingoTemplate template) {
        bingoTemplateRepository.save(template);
        return template;
    }
}
