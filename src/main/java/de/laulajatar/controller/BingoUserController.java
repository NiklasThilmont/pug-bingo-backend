package de.laulajatar.controller;

import de.laulajatar.StreamUtils;
import de.laulajatar.model.user.BingoUser;
import de.laulajatar.persistence.BingoUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@RestController
@RequestMapping("/user")
public class BingoUserController {


    private final BingoUserRepository bingoUserRepository;

    @Autowired
    public BingoUserController(BingoUserRepository bingoUserRepository) {
        this.bingoUserRepository = bingoUserRepository;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/{name}")
    public ResponseEntity<BingoUser> getByName(@PathVariable("name") String name) {
        Optional<BingoUser> user = StreamUtils.asStream(bingoUserRepository.findAll().iterator())
                .filter(bingoUser -> bingoUser.getName().equals(name))
                .findFirst();
        return user.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.ok(null));
    }

    @RequestMapping(method = RequestMethod.POST, path = "")
    public ResponseEntity<BingoUser> create(@RequestBody BingoUser user) {
        Optional<BingoUser> concurrentUser = StreamUtils.asStream(bingoUserRepository.findAll().iterator())
                .filter(bingoUser -> bingoUser.getName().equals(user.getName()))
                .findFirst();
        return concurrentUser.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.ok(bingoUserRepository.save(user)));
    }

}
