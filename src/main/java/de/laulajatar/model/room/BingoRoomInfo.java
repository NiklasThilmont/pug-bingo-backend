package de.laulajatar.model.room;

/**
 * Created by Niklas on 01.06.2017.
 */
public class BingoRoomInfo {
    public String id;
    public String roomName;
    public Boolean hasPassword;

    public BingoRoomInfo() {
        this.id = null;
        this.roomName = null;
        this.hasPassword = false;
    }

    public BingoRoomInfo(BingoRoom room) {
        this.id = room.getId();
        this.roomName = room.getRoomName();
        this.hasPassword = room.hasPassword();
    }
}
