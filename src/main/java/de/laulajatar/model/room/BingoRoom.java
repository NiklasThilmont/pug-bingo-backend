package de.laulajatar.model.room;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.laulajatar.model.user.BingoUser;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import java.util.*;
import java.util.stream.Collectors;

@RedisHash("rooms")
public class BingoRoom {
    @Id
    private String id;
    private String roomName;
    /**
     * We do not want the password outside.
     */
    private String password;
    /**
     * These are the terms. A new user to the room gets these terms randomly distributed
     * over his own on entering.
     */
    private List<String> terms;
    @JsonIgnore
    private Set<String> calledTerms;
    private List<BingoParticipant> participantList = new ArrayList<>();
    private Set<String> winnerIds;

    public BingoRoom() {
        this.participantList = new ArrayList<>();
        this.winnerIds = new HashSet<>( );
        this.calledTerms = new HashSet<>();
    }

    public BingoRoom(String roomName, String password, List<String> terms) {
        this.roomName = roomName;
        this.password = password;
        this.terms = terms;
        this.participantList = new ArrayList<>();
        this.winnerIds = new HashSet<>();
        this.calledTerms = new HashSet<>();
    }

    /**
     * Defines if the room may be accessed with the given password
     * TODO make it password hash based.
     * @param password
     * @return
     */
    public boolean mayAccess(String password) {
        String roomPass = this.password == null ? "": this.password.trim();
        String accessPass = password == null ? "" : password.trim();
        return roomPass.equals(accessPass);
    }

    public boolean hasPassword() {
        return this.password != null && !this.password.trim().isEmpty();
    }

    /**
     * Calls the term for all participants
     * @param term
     */
    public void callTermToAllParticipants(String term) {
        this.calledTerms.add(term);
        participantList.forEach(bingoParticipant -> {
            bingoParticipant.callTerm(term);
            if(bingoParticipant.getBingoBoard().getBingo()) {
                winnerIds.add(bingoParticipant.getBingoUserId());
            }
        });

    }

    /**
     * Adds a user to the room.
     */
    public void addUserToRoom(BingoUser user) {
        if(participantList == null) participantList = new ArrayList<>();
        BingoBoard bingoBoard = new BingoBoard(terms.stream().map(s -> new BingoField(false, s)).collect(Collectors.toList()), false);
        BingoParticipant bingoParticipant = new BingoParticipant(user, bingoBoard);
        participantList.add(bingoParticipant);
        this.calledTerms.forEach(bingoParticipant::callTerm);
    }

    public boolean userParticipates(BingoUser user) {
        Optional<BingoParticipant> participant = this.participantList.stream().filter(bp -> bp.getBingoUserId().equals(user.getId())).findFirst();
        return participant.isPresent();
    }


    public void removeUserFromRoom(String userId) {
        this.participantList.removeIf(bingoParticipant -> bingoParticipant.getBingoUserId().equals(userId));
    }

    // == Getter/Setter == //

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<String> getTerms() {
        return terms;
    }

    public void setTerms(List<String> terms) {
        this.terms = terms;
    }

    public List<BingoParticipant> getParticipantList() {
        return participantList;
    }

    public void setParticipantList(List<BingoParticipant> participantList) {
        this.participantList = participantList;
    }

    public Set<String> getWinnerIds() {
        return winnerIds;
    }

    public void setWinnerIds(Set<String> winnerIds) {
        this.winnerIds = winnerIds;
    }
}
