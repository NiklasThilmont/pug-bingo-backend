package de.laulajatar.model.room;

import de.laulajatar.model.user.BingoUser;

/**
 * A user participating in a {@link BingoRoom}
 */
public class BingoParticipant {
    /**
     * The users bingo board.
     */
    private BingoBoard bingoBoard;

    /**
     * Reference to the bingo user
     */
    private String bingoUserId;

    private String bingoUserName;

    BingoParticipant() {

    }

    BingoParticipant(BingoUser bingoUser, BingoBoard bingoBoard) {
        this.bingoBoard = bingoBoard;
        this.bingoUserId = bingoUser.getId();
        this.bingoUserName = bingoUser.getName();
    }


    BingoParticipant(BingoBoard bingoBoard) {
        this.bingoBoard = bingoBoard;
    }

    public void callTerm(String term) {
        this.bingoBoard.callTerm(term);
    }

    public BingoBoard getBingoBoard() {
        return bingoBoard;
    }

    public void setBingoBoard(BingoBoard bingoBoard) {
        this.bingoBoard = bingoBoard;
    }

    public String getBingoUserId() {
        return bingoUserId;
    }

    public void setBingoUserId(String bingoUserId) {
        this.bingoUserId = bingoUserId;
    }

    public String getBingoUserName() {
        return bingoUserName;
    }

    public void setBingoUserName(String bingoUserName) {
        this.bingoUserName = bingoUserName;
    }
}
