package de.laulajatar.model.room;

import java.util.function.Predicate;

/**
 * Created by Niklas on 30.05.2017.
 */
public class BingoField {
    private boolean called;

    private String description;

    BingoField() {

    }

    public BingoField(String description) {
        this.description = description;
    }

    public BingoField(boolean called, String description) {
        this.called = called;
        this.description = description;
    }

    public boolean isCalled() {
        return called;
    }

    public void setCalled(boolean called) {
        this.called = called;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
