package de.laulajatar.model.room;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Bingo board represented by a list of {@link BingoField}.
 * <p>
 *     Each {@link BingoField}s position in the {@link BingoBoard#fieldList} represents its position
 *     on the 5x5 field, starting with 0 at the top left corner, extending right to bottom in the following
 *     manner:
 *     <br/>
 *     (00)(01)(02)(03)(04)<br/>
 *     (05)(06)(07)(08)(09)<br/>
 *     (10)(11)(12)(13)(14)<br/>
 *     (15)(16)(17)(18)(19)<br/>
 *     (20)(21)(22)(23)(24)<br/>
 * </p>
 * <p>
 *     {@link BingoField} may be called by using {@link BingoBoard#callTerm(String)}. This will mutate
 *     the list of bingo fields.
 * </p>
 */
public class BingoBoard {
    private List<BingoField> fieldList;

    private Boolean isBingo;

    BingoBoard() {
        // Keep empty
    }

    public BingoBoard(List<BingoField> fieldList, Boolean isBingo) {
        this.fieldList = fieldList;
        Collections.shuffle(this.fieldList);
        this.isBingo = isBingo;
    }



    private boolean getIsCalled(int x, int y) {
        int index = y * 5 + x;
        return fieldList.get(index).isCalled();
    }

    private boolean calculateRowBingo(int rowNumber) {
        boolean res = true;
        for(int x = 0; x <= 4; x++) {
            res = res && getIsCalled(x, rowNumber);
        }
        return res;
    }

    private boolean calculateColBingo(int colNumber) {
        boolean res = true;
        for(int y = 0; y <= 4; y++) {
            res = res && getIsCalled(colNumber, y);
        }
        return res;
    }

    private boolean calculateDiagonalBingo() {
        boolean res1 = true;
        boolean res2 = true;
        for(int x = 0, y = 0; x <= 4 && y <= 4; x++, y++) {
            res1 = res1 && getIsCalled(x, y);
        }
        for(int x = 0, y = 4; x <= 4 && y >= 0; x++, y--) {
            res2 = res2 && getIsCalled(x, y);
        }
        return res1 || res2;
    }

    private void calculateBingo() {
        boolean bingo = false;
        for(int row = 0; row <= 4 && !bingo; row++) {
            bingo = calculateRowBingo(row);
        }
        for(int col = 0; col <= 4 && !bingo; col++) {
            bingo = calculateColBingo(col);
        }
        bingo = bingo || calculateDiagonalBingo();
        this.isBingo = bingo;
    }

    /**
     * Calls all fields with the given description. Only calls out the term when
     * {@link BingoBoard#isBingo} is {@code false}, meaning no bingo has been done.
     * @param term the term to be
     */
    public void callTerm(String term) {
        if(!isBingo) {
            this.fieldList.forEach(bingoField -> {
                if(bingoField.getDescription().equals(term)) {
                    bingoField.setCalled(true);
                }
            });
            calculateBingo();
        }
    }


    public List<BingoField> getFieldList() {
        return fieldList;
    }

    public void setFieldList(List<BingoField> fieldList) {
        this.fieldList = fieldList;
    }

    public Boolean getBingo() {
        return isBingo;
    }

    public void setBingo(Boolean bingo) {
        isBingo = bingo;
    }
}
