package de.laulajatar.model.user;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

@RedisHash("bingouser")
public class BingoUser {
    @Id
    private String id;
    private String name;
    private String key;

    public BingoUser() {
    }

    public BingoUser(String id, String name, String key) {
        this.id = id;
        this.name = name;
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
