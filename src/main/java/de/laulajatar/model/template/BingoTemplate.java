package de.laulajatar.model.template;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import java.util.List;

/**
 * Serves as a template and wraps a list of 25 strings.
 */
@RedisHash("template")
public class BingoTemplate {
    @Id
    private String id;
    private String name;
    private List<String> terms;


    public List<String> getTerms() {
        return terms;
    }

    public void setTerms(List<String> terms) {
        this.terms = terms;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
