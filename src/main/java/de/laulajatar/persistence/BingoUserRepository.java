package de.laulajatar.persistence;

import de.laulajatar.model.user.BingoUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.stream.Stream;

@Repository
public interface BingoUserRepository extends CrudRepository<BingoUser, String> {
}
