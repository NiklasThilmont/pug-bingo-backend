package de.laulajatar.persistence;

import de.laulajatar.model.template.BingoTemplate;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BingoTemplateRepository extends CrudRepository<BingoTemplate, String> {
}
