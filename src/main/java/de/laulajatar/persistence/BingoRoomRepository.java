package de.laulajatar.persistence;

import de.laulajatar.model.room.BingoRoom;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BingoRoomRepository extends CrudRepository<BingoRoom, String> {
}
