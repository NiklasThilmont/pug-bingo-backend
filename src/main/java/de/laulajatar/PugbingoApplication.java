package de.laulajatar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import redis.clients.jedis.JedisPoolConfig;

@SpringBootApplication
@EnableRedisRepositories
public class PugbingoApplication {

    @Bean
    public RedisConnectionFactory redisConnectionFactory() {
        final JedisConnectionFactory factory = new JedisConnectionFactory();
        factory.setHostName("localhost");
        factory.setDatabase(0);
        return factory;
    }

    @Bean
    public RedisTemplate<?, ?> redisTemplate() {
        RedisTemplate<byte[],byte[]> res =  new RedisTemplate<byte[], byte[]>();
        res.setConnectionFactory(redisConnectionFactory());
        return res;
    }


	public static void main(String[] args) {
		SpringApplication.run(PugbingoApplication.class, args);
	}
}
